const express = require('express');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const cors = require('cors');
//const { request } = require('express');

const app =express();
 const mongoose=require('mongoose')


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
const bcrypt=require('bcrypt');
const alert=require('alert')

mongoose.connect('mongodb://localhost/reactcrud-try_4',{useNewUrlParser:true,useUnifiedTopology:true});



// app.post('/user',function(req,res){

//     var name=req.body.name;
//     var email= req.body.email;

//     var mydata=mongoose.Schema({
//         name:String,
//         email:String
//     })
//     var User=mongoose.model('User',mydata,'users');
//     var User1=new User({name:name,email:email})
//     User1.save(function(err,data){
//         if(err)
//         throw err;
//         else{
//             res.send(data);
//         }

//     })
// })

 app.get('/', (req,res) => {
     res.send('welcome to my forms')
 })



//api for get data from database
// app.get("/getdata",function(req,res){ 
//     model.find({},function(err,data){
//                if(err){
//                    res.send(err);
//                }
//                else{           
//                    res.send(data);
//                    }
//            });
//    })

// app.post("/logindata",function(req,res)
// {
//     if(err)
//     throw err;
//     if(!res)
//     console.log("no user exists")

// })



var Schema=mongoose.Schema;

    var mydata = new Schema({FirstName: String, LastName: String,FatherName:String, MotherName: String,
        DateOfBirth:Date,
        Gender:String,
        EmailId:String,
        PhoneNo: Number,
        AltContactNo:Number,
        Course:String,
        Department:String,
        Semester:String,
        Address:String,
        City:String,
        State:String,
        CreatePassword:String


    })
    
    var model = mongoose.model('model', mydata, 'student');

app.post("/savedata",function(req,res)
{

    const saltPassword = bcrypt.genSaltSync(10);
    const securePassword= bcrypt.hashSync(req.body.CreatePassword,saltPassword);
    var FirstName=req.body.FirstName;
    var LastName=req.body.LastName;
    var FatherName=req.body.FatherName;
    var MotherName=req.body.MotherName;
    var DateOfBirth=req.body.DateOfBirth;
    var Gender=req.body.Gender;
    var EmailId=req.body.EmailId;
    var PhoneNo=req.body.PhoneNo;
    var AltContactNo=req.body.AltContactNo;
    var Course=req.body.Course;
    var Department=req.body.Department;
    var Semester=req.body.Semester;
    var Address=req.body.Address;
    var City=req.body.City;
    var State=req.body.State;
    var CreatePassword=securePassword;


// var Schema=mongoose.Schema;

//     var mydata = new Schema({FirstName: String, LastName: String,FatherName:String, MotherName: String,
//         DateOfBirth:Date,
//         Gender:String,
//         EmailId:String,
//         PhoneNo: Number,
//         AltContactNo:Number,
//         Course:String,
//         Department:String,
//         Semester:String,
//         Address:String,
//         City:String,
//         State:String,
//         CreatePassword:String


//     })
    
//     var model = mongoose.model('model', mydata, 'student');
    model.findOne({EmailId:EmailId},function(err,doc)
{
    if(err) 
    throw err;
    if(doc)
    {
    //console.log("User already exists")
    alert('Username already exists')
    }
    if(!doc)
    {
    alert('Succesfully Registered')
    var User1=new model({FirstName:FirstName,LastName:LastName,FatherName:FatherName, MotherName: MotherName,DateOfBirth:DateOfBirth,
        Gender:Gender,EmailId:EmailId,PhoneNo:PhoneNo,AltContactNo:AltContactNo,Course:Course,
        Department:Department,Semester:Semester,Address:Address,City:City,State:State,
    CreatePassword:CreatePassword})
    User1.save(function(err,data){
                 if(err){
                         res.send(err);              
                     }
                     else{      
                          res.send(data);
                     }
    }
    )
}
})
})

app.get("/getdata",(req,res)=>
{
    // var Schema=mongoose.Schema;
    // var mydata = new Schema({FirstName: String, LastName: String,FatherName:String, MotherName: String,
    //     DateOfBirth:Date,
    //     Gender:String,
    //     EmailId:String,
    //     PhoneNo: Number,
    //     AltContactNo:Number,
    //     Course:String,
    //     Department:String,
    //     Semester:String,
    //     Address:String,
    //     City:String,
    //     State:String,
    //     CreatePassword:String


    // })
    
    // var User1 = mongoose.model('User1', mydata,'student');

    model.find({},function(err,users)
    {
        if(err)
        throw err;
        else
        {
        console.log(users);
        res.send(users)
       
        
        }
    })
})




// app.post("/api/savedata",function(req,res){ 
     
//     var mod = new model(req.body);
//         mod.save(function(err,data){
//             if(err){
//                 res.send(err);              
//             }
//             else{      
//                  res.send({data:"Record has been Inserted..!!"});
//             }
//         });
// })

app.post('/contactform', (req, res) => {
    
    let data = req.body
    let smtpTransport = nodemailer.createTransport({
        service: 'Gmail',
        port: 465,
        auth: {
            user: 'nodemailer61@gmail.com',
            pass: 'nodemailer123'
        }
    });


    let mailOptions = {
        from: data.email,
        to: 'mahimas99@gmail.com',
        subject: `Message from ${data.firstName}`,
        html: `
        <h3>Information</h3>
        <ul>

        <li>Name: ${data.firstName}</li>
        <li>Email: ${data.email}</li>
        <li>Message: ${data.msg}</li>
        <li>Mobile: ${data.mobile}</li>
        </ul>
        `
    };
    smtpTransport.sendMail(mailOptions, (error, res) => {
        if (error) {
        res.send(error)
        }
        else {
            res.send('success')
        }
})
smtpTransport.close()
})

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
    console.log(`server running at port ${PORT}`);
})