import React, { Component } from 'react'
// import {Route,BrowserRouter as Router, Link} from 'react-router-dom'
import './App.css'
import CarouselComponent from './components/CarouselComponent'
import Footer from './components/Footer'
import HomeComponent from './components/HomeComponent'
import StudentDashboardComponent from './components/StudentDashboardComponent'

export class App extends Component {
  render() {
    return (
      <div className="App">
         
        <CarouselComponent />
        <HomeComponent />
         {/* <StudentDashboardComponent />  */}
        <br/>
        <Footer/>
 
      </div>
    )
  }
}

export default App