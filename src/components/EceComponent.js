import React, { Component } from 'react'
import ece from '../images/ece_1.jpg'
import '../css/foot.css'
import Footer from './Footer'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
export class EceComponent extends Component {
    render() {
        return (
            <div><Breadcrumb>
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
            <Breadcrumb.Item >Department</Breadcrumb.Item>
            <Breadcrumb.Item active>ece</Breadcrumb.Item>
          </Breadcrumb>
                <div className="container-fluid">
                    
                    <div class="media">
                        <img class="align-self-start mr-2 mt-5" src={ece} alt="image" style={{ width: "20%" }} />
                        
                        <div className="media-body">
                        <h2>Electronics and Communication Engineering</h2>
                            <h2 class="mt-2"> Vision</h2>
                                
                                <p>To establish the department as a center of excellence in academics and research with advances in the rapidly changing field of Electronics and Communication.</p>
                            
                             <h2>Mission</h2>
                                <p>To create stimulating environment for learning and imparting quality technical education to fulfill the needs of industry and society.</p>
                                
                                <p>The department was set-up in 2001 and twice accredited by AICTE-NBA. Well-equipped laboratories with advanced equipment worth more than Rs. 1.5 crore along with advanced software have helped to achieve excellence in design and research.
                                    The department has gained the distinction of producing merit holders with students appearing among the top ten University rankers. A forum called 'communique' has been set up by the department which provides a platform to the students and staff to showcase their talent
                                    through various technical,curricular and co-curricular activities.</p>
                            <h2>Salient Features of Department</h2>
                            <ul>
                                <li>Electronics and Communication Engineering Programme was established in year 2001 with running intake of 120 students. The department is the first preference of central students.</li>
                                <li>The Programme was accredited twice in the year 2008 and 2014 by National Board of Accreditation, New Delhi.</li>
                                <li>The Programme has twelve well equipped laboratories with state–of-the-art infrastructure around total investment of nearly 1.6 crores.</li>
                            </ul>
                        </div>
                    </div>
                    
                    
                </div>
                    
                <Footer/>
            </div>
          
            
        )
    }
}

export default EceComponent