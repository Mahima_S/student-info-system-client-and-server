import React, { Component } from 'react'
import '../css/studentdashboard.css'
import photo from '../images/studentReg_1.jpg'
import {Route,BrowserRouter as Router, Link,Switch} from 'react-router-dom'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
export class AdminProfile extends Component {
    render() {
        return (
            <div>
                 <Breadcrumb>
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
            <Breadcrumb.Item href="/login">Login</Breadcrumb.Item>
            <Breadcrumb.Item active>Admin Dasboard</Breadcrumb.Item>
                </Breadcrumb>
            <div class="stuprofile">
                
            <div class="row row-content">
            <div class="offset-10"><br/>
            {/* <Route>
                        <Link className="nav-link"   to="/"><button class="btn btn-primary"><em>Logout</em></button>   </Link> 
                        </Route> */}
                 <button class="btn btn-primary"><em>Logout</em></button>   
            </div>
                <div class="offset-1 col-lg-6" >
               
              
                   <div col-sm-12><h2>Admin Profile</h2>
                       <table class="table table-sm table-striped">
                           
                           <tbody>
                           <tr>
                                   <th>Admin Id</th>
                                   <td>1001</td>
                               </tr>
                               <tr>
                                   <th>Admin Name</th>
                                   <td>Caroline</td>
                              </tr>
                               <tr>
                                   <th>Admin Gender</th>
                                   <td>Female</td>
                               </tr>                                  
                               {/* <tr>
                                   <th>Department</th>
                                   <td></td>
                               </tr> */}
                               <tr>
                                   <th>Year of joining</th>
                                   <td>2018</td>
                               </tr>
                               
                               <tr>
                                   <th>Email</th>
                                   <td>caroline@gmail.com</td>
                               </tr>
                               <tr>
                                   <th>Phone</th>
                                   <td>987654321</td>
                               </tr>
                               <tr><th>
                                   City</th>
                                   <td>Chennai</td>
                               </tr>

                               
                           </tbody>
                       </table>
                   </div>

                    
               </div>
                <div class="offset-1 col-lg-4 ">
               
                    <div class="photo">
                        <div class="row ">  
                            <img src={photo} alt="Student Photo"></img>
                        </div>  
                    </div>

                    <div class="updatebutton">
                        <div class="row">
                            <button class="btn btn-primary btn-lg"><em>Update Profile</em></button>
                        </div>
                    </div> 
               
                </div>
                  
         
                <div>
                       
                          
                    </div> 
  
            </div>
            
        </div>
        </div>
       
        )
    }
}

export default AdminProfile