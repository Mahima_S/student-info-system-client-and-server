import React, { Component } from 'react'
import medal from '../images/medal.png'
import school from '../images/school.png'
import planet from '../images/planet-earth.png'
import teamwork from '../images/teamwork.png'
import '../css/home.css'
export class HomeComponent extends Component {
    render() {
        return (
            <div className="container-fluid" style={{ backgroundColor: "white", padding: "10px" }}>
                <div class="jumbotron jumbotron-fluid">
                    <div class="container-fluid">
                        <h1 style={{textAlign:"center"}}>ABC COLLEGE OF ENGINEERING</h1>
                        <p class="lead" style={{textAlign:"center"}}>The best place to learn Engineering</p>
                    </div>
                </div>

                <div className="container">
                <h2 style={{ color: "#007bff"}}>Vision</h2>
                    <p >
                        ABC College of Engineering and Management envisages the institute par excellence, providing world class technical and management education.

                    </p>
                <h2 style={{ color: "#007bff"}}>Mission</h2>
                    <p >
                        To impart quality education in the field of Engineering and Management and to foster mutually beneficial relationship with industries to create an intellectually stimulating environment for learning, 
                        research and for promoting professional and ethical values.
                    </p>

                <h2 style={{ color: "#007bff"}}>Autonomy</h2>
                    <p >
                        ABC was granted progressive academic autonomy from the session 2011-12. Various statutory bodies such as Board of Management, Academic Council, Board of Studies, and Finance Committee have been constituted and an
                        industry need-based syllabus has been introduced.
                    </p>
                </div>

                <div className="row">
                   
                   <div class="col-lg-3 col-sm-6 card"  >
                        <img class="card-img-top" src={medal} alt="Card image"/>
                            <div className="card-body" >
                                <h5 class="card-title">NIRF 2020</h5>
                                <p class="card-text">	

                            ABC has got INDIA RANKING in the range of '113' by National Institutional Ranking Framework in Engineering Category in 2020</p>
                
                            </div>
                    </div>

                    <div class="col-lg-3 col-sm-6 card" >
                        <img class="card-img-top" src={school} alt="Card image" />
                            <div className="card-body" >
                                <h5 class="card-title">Proud Heritage</h5>
                                <p class="card-text">Achieving excellence and polishing sharp minds for more than 35 years</p>
                
                            </div>
                        
                    </div>

                    <div class=" col-lg-3 col-sm-6 card" >
                        <img class="card-img-top" src={planet} alt="Card image"/>
                            <div className="card-body" >
                                <h5 class="card-title">International Collaborations</h5>
                                <p class="card-text">MoUs with reputed Universities of USA, UK, and Australia</p>
               
                            </div>
                    </div>
                    
                    <div class=" col-lg-3 col-sm-6 card">
                        <img class="card-img-top" src={teamwork} alt="Card image" />
                            <div className="card-body" >
                                <h5 class="card-title">Consistently<br/> Prolific Placements</h5>
                                <p class="card-text">Average placements for last 10 years is more than 80%.</p>
               
                            </div>
                    </div>
            
           
                </div>
                
            </div>
        )
    }
}

export default HomeComponent