import React, { Component } from 'react'
//import MapContainer from './MapContainer'
import '../css/contact.css'
import Footer from './Footer';
import '../css/foot.css'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import axios from 'axios'
export class ContactComponent extends Component {
    constructor(props) {
        super(props); 
        this.state = {
          fields: {
            firstName: "",
            email: "",
            mobile: "",
            msg:""
          },
          errors: {
            firstName: "",
            email: "",
            mobile: "",
          
          }
        };
        }
        validate = (name, value) => {
          const { fields } = this.state;
          switch (name) {
            case "firstName":
              if (!value || value.trim() === "") {
                return "Name is Required";
              } else if(Number(value)) {
                return "Name should contain letters only";
              }
              else {
                return "";
              }
            case "email":
              if (!value) {
                return "Email ID is Required";
              } else if (
                !value.match(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/)
              ) {
                return "Enter a valid email address";
              } else {
                return "";
              }
            case "mobile":
              if (!value || value.trim() === "") {
                return "Mobile number is Required";
              } else if (!value.match(/^[6-9]\d{9}$/)) {
                return "Enter a valid mobile number.";
              } else {
                return "";
              }
            
            default: {
              return "";
            }
          }
        };
        handleUserInput = e => {
          this.setState({
            errors: {
              ...this.state.errors,
              [e.target.name]: this.validate(e.target.name, e.target.value)
            },
            fields: {
              ...this.state.fields,
              [e.target.name]: e.target.value
            }
          });
        };
        handleSubmit = e => {
          const { fields } = this.state;
          e.preventDefault();
          let validationErrors = {};
          Object.keys(fields).forEach(name => {
            const error = this.validate(name, fields[name]);
            if (error && error.length > 0) {
              validationErrors[name] = error;
            }
          });
          if (Object.keys(validationErrors).length > 0) {
            this.setState({ errors: validationErrors });
            return;
          }
          if (fields.firstName && fields.email && fields.mobile && fields.msg) {
            const data = {
              firstName: fields.firstName,
              email: fields.email,
             
              mobile: fields.mobile,
              msg: fields.msg
            };
            window.alert("Sent message successfully", JSON.stringify(data));
            console.log("----data----", data);
          }
           
        let data={
          firstName:this.state.fields.firstName,
          email:this.state.fields.email,
         
          mobile:this.state.fields.mobile,
          msg:this.state.fields.msg,

        }
        axios.post('/contactform',data).then
        (response=>{
          this.setState({
            sent:true,
          },this.resetForm())
        })
        .catch(()=>{
          console.log("message not sent")
        })   
      };

      resetForm=(e) =>{
        this.setState({
            firstName: '',
            email:'',
            mobile:'',
            msg:''
        })
        setTimeout(() => {
            this.setState({
                sent:false,
            })
        },3000)
    }

  render() {
    const { fields, errors } = this.state;
      return (
        <div>
            <Breadcrumb>
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
            
            <Breadcrumb.Item active>Contact</Breadcrumb.Item>
          </Breadcrumb>
          <div className="container-fluid">
          
          <div className="row">
                 <div className="offset-1 col-lg-4 col-sm-12">
                    <h2>Drop us a Message</h2>
                      <form style={{backgroundColor:"whitesmoke" ,padding:"10px",borderRadius:"3%"}}>
                        
                        <div className="form-group">
                          <label for="firstName">Full Name <span className="asterisk" style={{color:"red"}}> *</span></label> 
                          <input
                            type="text"
                            name="firstName" 
                            className="form-control"
                            value={fields.firstName}
                            onChange={event => this.handleUserInput(event)}
                            placeholder="Enter your name"
                        />
                        </div>
                        <div>
                            <span className="text-danger">{errors.firstName}</span>
                        </div>
                        
                
                        <div className="form-group">
                          <label for="email">Email ID <span className="asterisk" style={{color:"red"}}> *</span></label> 
                          <input
                          type="email"
                          name="email"
                    value={fields.email}
                    className="form-control"
                          onChange={event => this.handleUserInput(event)}
                          placeholder="Enter your mail id"
                        />
                        <div>
                          <span className="text-danger">{errors.email}</span>
                        </div>
                        </div>
                
                        <div className="form-group">
                          <label for="mobile">Contact Number<span className="asterisk" style={{color:"red"}}> *</span></label>
                            <input
                            name="mobile"
                    value={fields.mobile}
                    className="form-control"
                            onChange={event => this.handleUserInput(event)}
                            placeholder="Enter your mobile number"
                          />
                          <div>
                            <span className="text-danger">{errors.mobile}</span>
                          </div>
                        </div>
                
                        <div className="form-group">
                          <label for="msg">Message:</label> 
                    <textarea className="form-control"
                      value={fields.msg}
                      rows="5" name="msg" onChange={event => this.handleUserInput(event)}></textarea>
                        </div>
                
                        <div className="form-group" style={{textAlign:"center"}}>
                        <button
                                type="button"
                                className="btn btn-primary"
                                onClick={this.handleSubmit}
                                >Submit</button> 
                        </div>
                      </form>
                      <br/>
                  </div>
            
                  <div className="offset-1 col-lg-5 col-sm-12">
                    <h2 style={{backgroundColor:"white"}}>Contact Information</h2>
                    <div >
                        
                      <strong>Address</strong><br/>
                      {/* <br/> Block 4, DLF IT Park, <br/>
                      Mount Poonamalle High Road, Chennai,<br /> 
                      Tamil Nadu 600089<br /> */}
                      No: 123, Abc Street, Main Road<br/>
                      <strong>Email</strong>
                      <br />abc@gmail.com<br/>
                      <strong>Contact number:</strong>
                      <br />044-22113344<br />
                      <br/>
                      {/* <MapContainer/> */}
                <iframe className="col-sm-12" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3887.23563496978!2d80.17324481343049!3d13.02066111732613!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a5260ccced29253%3A0x555884817befe480!2sL%26T%20Infotech!5e0!3m2!1sen!2sin!4v1609850278162!5m2!1sen!2sin"
                  width="400" height="350" frameborder="0" style={{ "border": 0 }}
                      allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
              
                    
                  </div>
                 
                
            </div>
              
          </div>
          <Footer/> 
        </div>
        
            
        )
    }
}

export default ContactComponent