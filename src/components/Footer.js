import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../css/foot.css'
export class Footer extends Component {
    render() {
        return (
          <div>
            <footer>
              <div class="main-content bg-primary">
                <div class="left box">
                  <h2> About us</h2>
                    <div class="content">
                      <p>ABC University is a home to aesthetically designed buildings with 
                        state of the-art computer and internet facilities, modern workshops, seminar halls, auditoriums and well stocked libraries,
                        sports and games fields in addition to an indoor stadium with gymnasium
                      </p>
                      <div class="social">
                        <a href="https://facebook.com"><span class="fab fa-facebook-f"></span></a>
                        <a href="https://twitter.com"><span class="fab fa-twitter"></span></a>
                        <a href="https://instagram.com"><span class="fab fa-instagram"></span></a>
                        <a href="https://youtube.com"><span class="fab fa-youtube"></span></a>
                      </div>
                    </div>

                </div>

                <div className="right box">
                  <h2>Quick Links</h2>
                  <div className="content">
                  <ul className="list-unstyled">
                        <li><i class="fa fa-home fa-x" aria-hidden="true" style={{marginRight :"7px",color:"black"}}></i><Link className="nav-item"  to="/" >Home</Link></li>
                        <li><i class="fa fa-newspaper-o fa-1x" aria-hidden="true" style={{marginRight :"7px",color:"black"}}></i><Link className="nav-item" to="/news">News</Link></li>
                        {/* <li><i class="fa fa-building fa-1x" aria-hidden="true" style={{marginRight :"7px",color:"black"}}></i><Link className="nav-item" to="/departments">Department</Link></li> */}
                        <li><i class="fa fa-picture-o fa-1x" aria-hidden="true" style={{marginRight :"7px",color:"black"}}></i><Link className="nav-item" to="/gallery">Gallery</Link></li>
                        <li><i class="fa fa-phone fa-1x" aria-hidden="true" style={{marginRight :"7px",color:"black"}}></i><Link className="nav-item" to="contact">Contact</Link></li>
                    </ul>
                  </div>
                </div>

                <div class="center box">
                  <h2>Address</h2>
              
                  <div class="content">
                    <div class="place">
                      <span class="fas fa-map-marker-alt"></span>
                      <span class="text">No: 123, Abc Street, Main Road</span>
                    </div>

                    <div class="phone">
                      <span class="fas fa-phone-alt"></span>
                      <span class="text">044-22113344</span>
                    </div>

                    <div class="email">
                      <span class="fas fa-envelope"></span>
                      <span class="text">abc@gmail.com</span>
                    </div>
                  </div>
                </div>

              </div>
            </footer>
          </div>
               

        )
    }
}

export default Footer