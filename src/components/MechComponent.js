import React, { Component } from 'react'
import mechanical from '../images/mechanical_1.jpg'
import '../css/foot.css'
import Footer from './Footer'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
export class MechComponent extends Component {
    render() {
        return (
            <div><Breadcrumb>
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
            <Breadcrumb.Item >Department</Breadcrumb.Item>
            <Breadcrumb.Item active>mechanical</Breadcrumb.Item>
          </Breadcrumb>
                <div className="container-fluid">
                    
                    <div class="media">
                        <img class="align-self-start mr-2 mt-5" src={mechanical} alt="image" style={{ width: "20%" }} />
                        
                        <div className="media-body">
                        <h2>Mechanical Engineering</h2>
                            <h2 class="mt-2"> Vision</h2>
                                
                                <p>Department of Mechanical Engineering aims to inculcate in students flair for excellence to become technological leader in industry and society.</p>
                            
                             <h2>Mission</h2>
                                <p>To create the learning environment that stimulates students & faculty to enhance the knowledge in Mechanical Engineering.</p>
                                
                                <p>TTo prepare the students to carry out research intended to cater the needs of the industry and society.</p>
                            
                            <h2>Salient Features of Department</h2>
                            <ul>
                                <li>The Department of Mechanical Engineering is offering B.E in Mechanical Engineering since 2009.</li>
                                <li>Well qualified and dynamic teaching faculty with 10 PhD’s having average teaching experience of 15 years.</li>
                                <li>More than 90 Research Publications in National, International Journals and Conferences.</li>
                            </ul>
                        </div>
                    </div>
                    
                    
                </div>
                    
                <Footer/>
            </div>
        )
    }
}

export default MechComponent