import React, { Component } from 'react'
 import '../css/admin.css'
 import {Route,BrowserRouter as Router, Link,Switch} from 'react-router-dom'
import StudentDashboardComponent from './StudentDashboardComponent'
import AdminStudentDashboard from './AdminStudentDashboard'
import AdminProfile from './AdminProfile'
import medal from '../images/medal.png'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import axios from 'axios'
export class AdminComponent extends Component {

  handleClick= e =>
  {
axios.get('http://localhost:3001/getdata').then(response=> {
  this.setState({
      sent:true,
  })
})
  }
  
    
  
  render() {

    
    
    return (
      <div >
         <Breadcrumb>
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
            <Breadcrumb.Item href="/login">Login</Breadcrumb.Item>
            <Breadcrumb.Item active>Admin Dashboard</Breadcrumb.Item>
        </Breadcrumb>
     <h1 style={{alignItems:"center"}}>Admin Dashboard</h1>    
    <Router>
     
         
          {/* <ul className="nav navbar-nav">
       <li className="nav-item">
                <Link className="nav-link"   to="/studentdetails">Student details</Link>
              </li>
           
            <li className="nav-item">
                <Link className="nav-link"   to="/adminprofile">Admin Profile</Link>
              </li> 
              </ul>  */}
          <div className="row">
           <div class="offset-lg-3 col-lg-3 col-sm-6 card"  >
          <Link className="nav-link"   to="/studentdetails">  <img class="card-img-top" src={medal} alt="Card image"/></Link>     
                            <div className="card-body" >
                            <Link className="nav-link"   to="/studentdetails"> <h5 class="card-title">STUDENT DETAILS</h5></Link>
                                
              
                                <p class="card-text">	</p>
                
                            </div>
          </div> 
          <div className="card">
            <div className="card-body">
          <button className="viewbtn" style={{padding:"2%",margin:"2%"}}onClick={event => this.handleClick(event)}>View Student details</button>
          </div>
          </div>
          
          <div class="offset-1 col-lg-3 col-sm-6 card"  >
          <Link className="nav-link"   to="/adminprofile"><img class="card-img-top" src={medal} alt="Card image"/></Link>
                            <div className="card-body" >
                            <Link className="nav-link"   to="/adminprofile"> <h5 class="card-title">ADMIN PROFILE</h5></Link>   
              
                                <p class="card-text">	</p>
                
                            </div>
                    </div>
         
                
          
          </div>
          
             
       
          <Route exact path="/studentdetails" component={AdminStudentDashboard} />
          <Route exact path="/adminprofile" component={AdminProfile} />
    </Router>
       
       {/* <div className="row">
       <button className="offset-1 col-lg-5 col-sm-12 a1 primary">
       <img src={logo}  alt="logo" height="70%" width="55%" />
        <h3 className="offset-1">Student Details</h3> 
       </button>
       <button className="offset-1 col-lg-5 col-sm-12 a1 primary">
       <img src={logo}  alt="logo" height="70%" width="55%" />
        <h3 className="offset-1" >Admin Profile</h3> 
        </button>
       </div> */}
      </div>
    )
  }
}

export default AdminComponent