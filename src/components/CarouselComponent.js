import React, { Component } from 'react'
import Carousel from 'react-bootstrap/Carousel'
import pic1 from '../images/graduate_1.jpg'
import pic2 from '../images/library_1.jpg'
import pic3 from '../images/reading_1.jpg'

export class CarouselComponent extends Component {
    render() {
        return (
            <div>
                <Carousel>
  <Carousel.Item interval={2000}>
    <img
      className="d-block w-100" style={{height:"400px"}}
      src={pic1}
      alt="First slide"
    />
    <Carousel.Caption>
      <h3>First slide label</h3>
      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item interval={2000}>
    <img
      className="d-block w-100" style={{height:"400px"}}
      src={pic2}
      alt="Third slide"
    />
    <Carousel.Caption>
      <h3>Second slide label</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item interval={2000}>
    <img
      className="d-block w-100" style={{height:"400px"}}
      src={pic3}
      alt="Third slide"
    />
    <Carousel.Caption>
      <h3>Third slide label</h3>
      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>
            </div>
        )
    }
}

export default CarouselComponent
