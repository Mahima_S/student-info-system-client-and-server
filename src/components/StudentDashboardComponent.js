import React, { Component } from 'react'
import '../css/studentdashboard.css'
import photo from '../images/profilepic_1.jpg'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import {Route,BrowserRouter as Router, Link,Switch} from 'react-router-dom'
import axios from 'axios'
export class StudentDashboardComponent extends Component {

    download() {
        axios
        (
            {
                url:'https://source.unsplash.com/random',
                method:'GET',
                responseType:'blob'

            }
        )
        .then((response) => {
            const url = window.URL.createObjectURL(new Blob([response.data]));
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', 'file.jpg'); //or any other extension
            document.body.appendChild(link);
            link.click();
         });
        
    }

    render() {
        return (
            <div>
                <Breadcrumb>
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
            <Breadcrumb.Item href="/login">Login</Breadcrumb.Item>
            <Breadcrumb.Item active>Student Dashboard</Breadcrumb.Item>
                </Breadcrumb>
                <div class="stuprofile">
                
                <div class="row row-content">
                    <div class="offset-10"><br />
                        <Route>
                        <Link className="nav-link"   to="/"><button class="btn btn-primary"><em>Logout</em></button>   </Link> 
                        </Route>
                
                </div>

                <div class="stuNavBar bg-primary offset-1 col-lg-10">
                    <button class="nav-btn btn btn-primary" onClick={this.download} style={{padding:"2%"}}>Course Details</button>
                    <button class="nav-btn btn btn-primary" onClick={this.download} style={{padding:"2%"}}>Study Material</button>
                    <button class="nav-btn btn btn-primary" onClick={this.download} style={{padding:"2%"}}>Time Table</button>
                </div>
                <br></br>
                <br></br>
                <br></br>
                    <div class="offset-1 col-lg-6" >
                   
                  
                       <div col-sm-12><h2>Student Profile</h2>
                           <table class="table table-sm table-striped">
                               
                               <tbody>
                                   <tr>
                                       <th>Student Name</th>
                                       <td>Amit</td>
                                  </tr>
                                   <tr>
                                       <th>Student Gender</th>
                                       <td>Male</td>
                                   </tr>
                                   <tr>
                                       <th>Course</th>
                                       <td>B.E.</td>
                                   </tr>
                                   <tr>
                                       <th>Department</th>
                                       <td>ECE</td>
                                   </tr>
                                   <tr>
                                       <th>Year</th>
                                       <td>4th</td>
                                   </tr>
                                   <tr>
                                       <th>Semester</th>
                                       <td>8th</td>
                                   </tr>
                                   <tr>
                                       <th>Roll Number</th>
                                       <td>55</td>
                                   </tr>
                                   <tr>
                                       <th>Student Email</th>
                                       <td>amit@sis.com</td>
                                   </tr>
                                   <tr>
                                       <th>Phone</th>
                                       <td>987654321</td>
                                   </tr>
                                   <tr><th>
                                       City</th>
                                       <td>Chennai</td>
                                   </tr>

                                   
                               </tbody>
                           </table>
                       </div>

                        
                   </div>
                    <div class="col-lg-4 ">
                   
                        <div class="photo">
                            <div class="row ">  
                                <img src={photo} alt="Student Photo"></img>
                            </div>  
                        </div>

                        <div class="offset-0 updatebutton">
                            <div class="row">
                                <button class="btn btn-primary btn-lg"><em>Update Profile</em></button>
                            </div>
                        </div> 
                   
                    </div>
                      
             
                   
      
                </div>
                
            </div>
           
            </div>
            
        )
    }
}

export default StudentDashboardComponent