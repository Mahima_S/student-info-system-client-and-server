import React from "react";
import "../css/carousel.css";
import Slider from "./Swiper";
import civil_1 from "../images/civil_1.jpg";
import cse_1 from "../images/cse_1.jpg";
import it_1 from "../images/it_1.jpg";
import ece_1 from "../images/ece_1.jpg";
import mechanical_1 from "../images/mechanical_1.jpg";
import { Card, CardImg, CardText, CardBody, CardTitle} from 'reactstrap';
import Footer from "../components/Footer";

export default function GalleryComponent() {
  const settings = {
    effect: "coverflow",
    centeredSlides: true,
    slidesPerView: 4,
    autoplay:true,
    coverflowEffect: {
      rotate: 0, // Slide rotate in degrees
      stretch: 60, // Stretch space between slides (in px)
      depth: 50, // Depth offset in px (slides translate in Z axis)
      modifier: 0, // Effect multipler
      slideShadows: true // Enables slides shadows
    },
    loop: true // Get swiper instance callback
  };

    return (
      <div>
            <div className="gallery">
        <h1>Culturals</h1>
      <Slider settings={settings}>
            
        <div className="">
            <CardImg className="card-image" top src={civil_1} alt="Card 1" />
         </div>
              
        <div>
            <CardImg className="card-image" top  src={cse_1} alt="Card 2" />
        </div>

        <div>
  
            <CardImg className="card-image" top  src={ece_1} alt="Card 3" />
     
    
        </div>

        <div>
  
            <CardImg className="card-image" top  src={it_1} alt="Card 4" />
      
    
        </div>

        <div>
  
            <CardImg className="card-image" top src={mechanical_1} alt="Card 5" />
      
   
        </div>
                 
          </Slider>
          <br/>
          <h1>Symposium</h1>
          <Slider settings={settings}>
            
            <div className="">
                <CardImg className="card-image" top src={civil_1} alt="Card 1" />
             </div>
                  
            <div>
                <CardImg className="card-image" top  src={cse_1} alt="Card 2" />
            </div>
    
            <div>
      
                <CardImg className="card-image" top  src={ece_1} alt="Card 3" />
         
        
            </div>
    
            <div>
      
                <CardImg className="card-image" top  src={it_1} alt="Card 4" />
          
        
            </div>
    
            <div>
      
                <CardImg className="card-image" top src={mechanical_1} alt="Card 5" />
          
       
            </div>
                     
              </Slider>
            </div>
            <Footer/>

      </div>
    
  );
}