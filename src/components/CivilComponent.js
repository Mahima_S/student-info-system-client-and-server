import React, { Component } from 'react'
import civil from '../images/civil_1.jpg'
import '../css/foot.css'
import Footer from './Footer'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
export class CivilComponent extends Component {
    render() {
        return (
            <div><Breadcrumb>
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
            <Breadcrumb.Item >Department</Breadcrumb.Item>
            <Breadcrumb.Item active>civil</Breadcrumb.Item>
          </Breadcrumb>
                <div className="container-fluid">
                    
                    <div class="media">
                        <img class="align-self-start mr-2 mt-5" src={civil} alt="image" style={{ width: "20%" }} />
                        
                        <div className="media-body">
                        <h2>Civil Engineering</h2>
                            <h2 class="mt-2"> Vision</h2>
                                
                                <p>To be a knowledge centre in civil engineering education, training, research, entrepreneurship and industry outreach services for creating sustainable infrastructure and enhancing quality of life.</p>
                            
                             <h2>Mission</h2>
                                <p>To generate quality civil engineers with strong technical and managerial skills through creation of conducive environment for creative learning and research in association with stake holders.</p>
                                
                            <p>Rapidly growing infrastructure demands for highly qualified competent Civil engineers. 
                                Considered as one of the oldest engineering disciplines, Civil Engineering involves planning, designing and executing infrastructural development works. The profession deals with a wide variety of tasks that envisages designing, supervision and construction activities of bridges, tunnels, buildings, airports, dams, water works, sewage systems, ports, etc. and offer a multitude of challenging career opportunities.
                                With privatization, liberalization, and globalization the scope of Civil Engineering has enhanced many folds.</p>
                            <h2>Salient Features of Department</h2>
                            <ul>
                                <li>The department was established in 1984, runs a Under-Graduate and two post graduate programmes namely M. Tech. (Structural Engineering) Full-Time and M. Tech. (Geotechnical Engineering) Part-Time.</li>
                                <li>PG program M. Tech. (Structural Engineering) is accredited twice by NBA, New Delhi. The details are:
                                    27/12/2016
                                    09/01/2019</li>
                                <li>The Department is also providing testing and consultancy services in the field of Geotechnical Engineering, Environmental / Waste water Engineering, Concrete Technology, Transportation Engineering, Strength of materials and Estimation and Surveying work with its well-equipped laboratories.</li>
                            </ul>
                        </div>
                    </div>
                    
                    
                </div>
                <Footer/>
            </div>
        )
    }
}

export default CivilComponent