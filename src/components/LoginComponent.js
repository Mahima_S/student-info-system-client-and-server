import React from "react";
import StudentDashboardComponent from '../components/StudentDashboardComponent'
import AdminComponent from '../components/AdminComponent'
import AdminStudentDashboard from '../components/AdminStudentDashboard'
import AdminProfile from '../components/AdminProfile'
import ReactDOM from 'react-dom';
import {Route,BrowserRouter as Router, Link,Switch} from 'react-router-dom'
import axios from "axios";
class LoginComponent extends React.Component {

  constructor(props) {
    super(props); 
    this.state = {
      fields: {
        // firstName: "",
        email: "",
        password: "",
        d1:"",
        sub:false
        },
      errors: {
        // firstName: "",
        email: "",
        password: "",
        }
    };

    this.onValueChange = this.onValueChange.bind(this);
  }

  validate = (name, value) => {
    const { fields } = this.state;
    switch (name) {
        case "email":
          if (!value) {
            return "Email ID is Required";
          } else if (
            !value.match(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/)
          ) {
            return "Enter a valid email address";
          } else {
            return "";
          }
      case "password":
        if (!value) {
          return "Password is Required";
        } else if (value.length < 8 || value.length > 15) {
          return "Please fill at least 8 character";
        } else if (!value.match(/[a-z]/g)) {
          return "Please enter at least lower character.";
        } else if (!value.match(/[A-Z]/g)) {
          return "Please enter at least upper character.";
        } else if (!value.match(/[0-9]/g)) {
          return "Please enter at least one digit.";
        } else {
          return "";
        }
          
     
      default: {
        return "";
      }
    }
  };

  handleUserInput = e => {
    this.setState({
      errors: {
        ...this.state.errors,
        [e.target.name]: this.validate(e.target.name, e.target.value)
      },
      fields: {
        ...this.state.fields,
        [e.target.name]: e.target.value
      }
    });
  };


  onValueChange(event) {
    this.setState({
      selectedOption: event.target.value
    });
  }

  handleSubmit = e => {
    const { fields } = this.state;
    e.preventDefault();
    let validationErrors = {};
    Object.keys(fields).forEach(name => {
      const error = this.validate(name, fields[name]);
      if (error && error.length > 0) {
        validationErrors[name] = error;
      }
    });
     if (Object.keys(validationErrors).length > 0) {
      this.setState({ errors: validationErrors });
      return;
    }
    if (fields.email && fields.password) {
      const data = {
        // firstName: fields.firstName,
        email: fields.email,
        password: fields.password,
        // mobile: fields.mobile
      };
      window.alert(this.state.selectedOption +" Logged in successfully", JSON.stringify(data));
      console.log("----data----", data);
    }
    var logindata={
      email:this.state.fields.email,
      password:this.state.fields.password
    }
  //   axios.post('http://localhost:3001/logindata', logindata)
  //   .then(response=> {
  //       this.setState({
  //           sent:true,
  //       })
  //   })
  //       .catch(() => {
  //       console.log("message not sent")
  //   })
  };

  render() {
    const { fields, errors } = this.state;
      return (
          <div className="container-fluid">
              <br />
                <h4 style={{textAlign:"center"}}>Choose your designation<br/>
                    <input type="radio" name="d1" value="Admin"
                   
                  onChange={this.onValueChange}
                  />&nbsp; Admin &nbsp;&nbsp;
                    <input type="radio" name="d1" value="Student" onChange={this.onValueChange} />&nbsp; Student
                </h4><br/>
              <div className="col-sm-12 offset-lg-4 col-lg-4 card" style={{textAlign:"center",width:"100%"}}>
                <form>
                    <div className="card-header">
                        <h3 style={{textAlign:"center",color:"#007bff"}}>Please enter your credentials below</h3>
                    </div>
                    <div className="card-body" style={{textAlign:"left"}}>
          
                    <label>Username<span className="asterisk" style={{color:"red"}}> *</span></label>
                        <input
                            type="text"
                            name="email" 
                            className="form-control"
                            value={fields.email}
                            onChange={event => this.handleUserInput(event)}
                            placeholder="Enter your username"
                        />
            
                        <div>
                            <span className="text-danger">{errors.email}</span>
                        </div>
                    
                      <br/>
         
                        <label>Password<span className="asterisk" style={{color:"red"}}> *</span></label>
                        <input
                        type="password"
                        name="password"
                        className="form-control"
                        value={fields.password}
                        onChange={event => this.handleUserInput(event)}
                        placeholder="Enter your password"
                        />
                        <div>
                             <span className="text-danger">{errors.password}</span>
                        </div>
                        <div>
                          <span><a href="https://google.com">Forgot Password?</a> </span>
                        </div>
         
                    </div>
        
                      <br />
                      <div className="card-footer" style={{ padding: "10px" }}>
                                <button
                                name="submitform"
                                type="button"
                                className="login-button pointer btn btn-primary"
                                onClick={(event)=>{this.handleSubmit(event);this.setState({sub:true})}}
                                
                                >Login</button> 
                      </div>
            </form>
           
             
           
              </div>
              
              {/* <AdminStudentDashboard></AdminStudentDashboard> */}

              {/* <AdminComponent></AdminComponent> */}
              {/* <AdminProfile></AdminProfile> */}
              {/* {this.state.sub && this.state.selectedOption==="Student" && this.state.errors.email==="" && this.state.errors.password===""? <StudentDashboardComponent></StudentDashboardComponent>:null}
              {this.state.sub && this.state.selectedOption==="Admin" && this.state.errors.email==="" && this.state.errors.password===""? <AdminComponent></AdminComponent>:null} */}
              {this.state.sub && this.state.selectedOption==="Student" && this.state.errors.email==="" && this.state.errors.password===""? this.props.history.push('/StudentDashboardComponent') :null}
              {this.state.sub && this.state.selectedOption==="Admin" && this.state.errors.email==="" && this.state.errors.password===""? this.props.history.push('/AdminComponent') :null}
        </div>
        
        
    );
  }
}
export default LoginComponent;