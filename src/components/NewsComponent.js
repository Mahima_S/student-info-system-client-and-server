import React, { Component } from 'react'
import '../css/news.css'
import '../css/foot.css'
import Footer from './Footer'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
export class NewsComponent extends Component {
    render() {
        return (
            <div>
                <Breadcrumb>
                    <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
  
                    <Breadcrumb.Item active>News</Breadcrumb.Item>
                </Breadcrumb>
                
                    <div className="row"><br/>
             <marquee>Complying to Govt. of Maharashtra 'MISSION BEGIN AGAIN' guidelines vide Order No. DMU/2020/CR.92/DisM-I, dated 30th September 2020; college will remain closed till further instructions. However Online / Distance learning is continued for the students.</marquee>
            
                <div class="offset-1 col-sm-12 col-lg-6 ">
                   
                        
                        <div class="card">
                            <div class="card-header bg-primary"  id="newshead" >
                            <h3 style={{color:"white"}}> News</h3>
                        </div>
                        <div  id="news" >
                            <div class="card-body" style={{marginLeft:"15px"}}>
                               
                                <ul>
                                    <li>Complying to Govt. of Maharashtra 'MISSION BEGIN AGAIN' guidelines vide Order No. DMU/2020/CR.92/DisM-I, dated 30th September 2020; college will remain closed till further instructions. However Online / Distance learning is continued for the students.</li>
                                    <li>ABC New IT Polilcy Read here</li>
                                    <li>Notice for Merit-cum-Means based Scholorship for Minority Community students for pursuing Technical and Professional Education 2020-21 See Notice</li>
                                    <li>UGC Public Notice - Redressal of Grievances Related to COVID-19 Pandemic Read here</li>
                                    <li>Wednesday 30.12.2020 declared as local holiday and compensated on 23.01.2021</li>
                                    </ul>
                                    
                            </div>
                        </div>
                </div>  
                <br/>
                <div class="card">
                    <div class="card-header bg-primary"  id="noticehead">
                    <h3 style={{color:"white"}}> Notice</h3>
                        </div>
                            <div  id="notice" >
                                <div class="card-body " style={{marginLeft:"15px"}}>
                                    <ul><em><li>Notice for options / preferences of Open Elective Courses for B.E. Semester IVth and VIth, 2020-21 of upcoming Even semester. See Notice</li>
                                    <li>Scholarship Notice for Academic Year 2021-22 for all courses     See Notice</li>
                                    <li>Complying to Govt. of Maharashtra 'MISSION BEGIN AGAIN' guidelines vide Order No. DMU/2020/CR.92/DisM-I, dated 30th September 2020; college will remain closed till further instructions. However Online / Distance learning is continued for the students.</li>
                                    <li>IMPORTANT NOTICE :: Notice on Promotion of students to next academic year 2020-21 for all Programmes     See Notice</li>
                                    <li>UGC Public Notice - Redressal of Grievances Related to COVID-19 Pandemic Read here</li>
                                    <li>ABC New IT Policy Read here</li>
                                    <li>Scholarship/ Incentive Schemes at Institute level See Notice</li>
                                    </em></ul>
                                </div>
                            </div>

                        </div><br />
                    
                <div class="card">
                        <div class="card-header bg-primary"  id="eventshead">
                            <h3 style={{color:"white"}}>Upcoming Events</h3>
                        </div>
                        <div  id="events" >
                            <div class="card-body" style={{marginLeft:"15px"}}>
                                <ul><em><li>New Training Courses from State-of-art CIIIT Center by the experts of ABC and TATA Technologies on various course of Product Design & Development, Advanced Integrated Manufacturing and Internet of Things (IoT)     Details    </li></em></ul>
                            </div>
                        </div>
                </div>  
                <br />
                
            </div>
            <div className="offset-1 col-sm-12 col-lg-3  ">
                <div className="card">
                <div className="card-header">
                  <h2>Admissions Open</h2>  
                </div>
                <div className="card-body" >
                    <ul style={{paddingLeft:"5%"}}> 
                        <li><a href="https://google.com">Admissions open for fulltime B.E./B.Tech 2021</a></li>
                        <li><a href="https://google.com">Online Applications for B.E / B.Tech (Lateral Entry) 2021 – OPEN</a></li>
                        <li><a href="https://google.com">Online Applications for B.E / B.Tech (Distance Learning Program) 2021 – OPEN</a></li>
                        <li><a href="https://google.com">Recruitment Of Junior Research Fellow 2021 – OPEN</a></li>
                    </ul>
                            
                
            </div>
            </div>
            <br></br>
            <br></br>
            <div className="card">
                <div className="card-header">
                <h2>Various Vacancies</h2>
                </div>
                <div className="card-body">
                    <ul style={{paddingLeft:"5%"}}>
                        <li><a href="https://google.com">Applications are invited for teaching staff-Click here for more detils</a></li>
                        <li><a href="https://google.com">Walk in interview for the post of chief admin-Apply before 15th January 2021 </a></li>
                        <li><a href="https://google.com">Apply for lab assistant positions-Click here for more details</a></li>
                        <li><a href="https://google.com">Applications are invited for the post of placement officer-Click here for more details</a></li>
                    </ul>

                </div>

            </div>
            </div>
            <div className>
                
            </div>
            
           
                </div>
                 <Footer/>
                 </div>   
                
        )
    }
}

export default NewsComponent
                