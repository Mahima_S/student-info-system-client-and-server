import React, { Component } from 'react'
import '../css/foot.css'
import '../css/register.css'
import Footer from './Footer'
import studentReg from '../images/studentReg_1.jpg'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import axios from 'axios'
export class RegisterComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
           fields:{ FirstName : '',
            LastName: '',
            FatherName:'',
            MotherName:'',
            DateOfBirth:'',
            Gender:'',
            EmailId:'',
            PhoneNo:null,
            AltContactNo:null,
            Course:'',
            Department:'',
            Semester:'',
            Address:'',
            City:'',
            State:'',
            CreatePassword:'',
            Re_enterPassword:'',
            sent:false
        },
           errors:{
            FirstName:'',
            LastName:'',
            FatherName:'',
            Mothername:'',
            doberrormessage:'',
            EmailId:'',
            PhoneNo:'',
            AltContactNo:'',
            Course:'',
            Address:'',
            City:'',
            State:'',
            CreatePassword:'',
            Re_enterPassword:''},
            profileImg:'https://pixabay.com/vectors/blank-profile-picture-mystery-man-973460/'

        };
    }



        validate = (name, value) => {
            const { fields } = this.state;
            switch (name) {
              case "FirstName":
                if (!value || value.trim() === "") {
                  return "First name is Required";
                }
                else if(Number(value))
                {
                   return "Name cannot contain numbers"
                } 
                else {
                  return "";
                }
             

                case "LastName":
                if (!value || value.trim() === "") {
                  return "Last name is Required";
                } 
                else if(Number(value))
                {
                   return "Name cannot contain numbers"
                } 
                
                else {
                  return "";
                }
             
                case "FatherName":
                if (!value || value.trim() === "") {
                  return "Father name is Required";
                } 
                else if(Number(value))
                {
                   return "Name cannot contain numbers"
                } 
                
                else {
                  return "";
                }

                case "MotherName":
                if (!value || value.trim() === "") {
                  return "Mother name is Required";
                } 
                else if(Number(value))
                {
                   return "Name cannot contain numbers"
                } 
                else {
                  return "";
                }

                case "EmailId":
                  
          if (!value) {
            return "Email ID is Required";
          } else if (
            !value.match(/^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/)
          ) {
            return "Enter a valid email address";
          } else {
            return "";
          }
                // if (!value || value.trim() === "") {
                //   return "Email Id is Required";
                // } 
                // else if(!value.includes("@")|| value[0]=="@")
                // {
                //     return "Provide a valid Email Id"
                // }
                // else {
                //   return "";
                // }


                case "PhoneNo":
                if (!value || value.trim() === "") {
                  return "Phone No. is Required";
                } 

                else if(!Number(value))
                {
                   return "Phone number contains only numbers"
                } 
                else if(value.length>10)
                {
                    return "Provide a valid Phone No."
                }
                
                else {
                  return "";
                }

                case "Address":
                if (!value || value.trim() === "") {
                  return "Address is Required";
                } else {
                  return "";
                }

                case "City":
                if (!value || value.trim() === "") {
                  return "City is required";
                } 
                else if(Number(value))
                {
                   return "City cannot contain numbers"
                }
                else {
                  return "";
                }

                case "State":
                if (!value || value.trim() === "") {
                  return "State is Required";
                } 
                else if(Number(value))
                {
                   return "State cannot contain numbers"
                }
                else {
                  return "";
                }

                case "CreatePassword":
                if (!value || value.trim() === "") {
                  return "Password is Required";
                } else {
                  return "";
                }

                case "Re_enterPassword":
                if (!value || value.trim() === "") {
                  return "Password is Required";
                } 
                
                else if(value!==this.state.fields.CreatePassword)
                {
                  return "Password doesn't match";
                }

                else {
                  return "";
                }

              default: {
                return "";
              }
            }
          };




          handleUserInput = e => {
            this.setState({
              errors: {
                ...this.state.errors,
                [e.target.name]: this.validate(e.target.name, e.target.value)
              },
              fields: {
                ...this.state.fields,
                [e.target.name]: e.target.value
              }
            });
          };


          handleSubmit = e => {
            const { fields } = this.state;
            e.preventDefault();
            let validationErrors = {};
            Object.keys(fields).forEach(name => {
              const error = this.validate(name, fields[name]);
              if (error && error.length > 0) {
                validationErrors[name] = error;
              }
            });
             if (Object.keys(validationErrors).length > 0) {
              this.setState({ errors: validationErrors });
              return;
            }
            if (fields.FirstName && fields.LastName && fields.FatherName &&fields.MotherName && fields.EmailId && fields.PhoneNo
                 && fields.Address && fields.City && fields.State 
                 && fields.CreatePassword && fields.Re_enterPassword && fields.DateOfBirth) {
              const data = {
                FirstName: fields.FirstName,
                LastName:fields.LastName,
                FatherName:fields.FatherName,
                MotherName:fields.MotherName,
                DateOfBirth:fields.DateOfBirth,
                Gender:fields.Gender,
                EmailId:fields.EmailId,
                PhoneNo:fields.PhoneNo,
                Address:fields.Address,
                City:fields.City,
                State:fields.State,
                CreatePassword:fields.CreatePassword,
                Re_enterPassword:fields.Re_enterPassword
                
              };
              // window.alert("submit success", JSON.stringify(data));
              console.log("----data----", data);
            }
            var registerdata=
            {
              FirstName:this.state.fields.FirstName,
              LastName:this.state.fields.LastName,
              FatherName:this.state.fields.FatherName,
              MotherName:this.state.fields.MotherName,
              DateOfBirth:this.state.fields.DateOfBirth,
              Gender:this.state.fields.Gender,
              EmailId:this.state.fields.EmailId,
              PhoneNo:this.state.fields.PhoneNo,
              AltContactNo:this.state.fields.AltContactNo,
              Course:this.state.fields.Course,
              Department:this.state.fields.Department,
              Semester:this.state.fields.Semester,
              Address:this.state.fields.Address,
              City:this.state.fields.City,
              State:this.state.fields.State,
              CreatePassword:this.state.fields.CreatePassword


            }
          


    axios.post('http://localhost:3001/savedata', registerdata)
    .then(response=> {
        this.setState({
            sent:true,
        },this.resetForm())
    })
        .catch(() => {
        console.log("message not sent")
    })
    
  
}
    resetForm=(e) =>{
        this.setState({
            FirstName: '',
           
        })
        setTimeout(() => {
            this.setState({
                sent:false,
            })
        },3000)
    }

          


    
     imageHandler = (e) =>{
        const reader = new FileReader();
        
        reader.onload = () => {
            
            if(reader.readyState === 2){
                this.setState({profileImg: reader.result})
            }
        }
        reader.readAsDataURL(e.target.files[0])
    }

  
    render() {
        const {profileImg}=this.state
        const { fields, errors } = this.state;
        return (
          <div>
            <Breadcrumb>
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
            
            <Breadcrumb.Item active>Register</Breadcrumb.Item>
          </Breadcrumb>
                <div className="container">
                <br/>
                <h2 style={{textAlign:"center"}}>Please fill the following details</h2>
                    <form style={{ backgroundColor: 'whitesmoke', width: "100%", padding: "20px" }}>
                       
                        <div className="page">
                            <div className="container">
                            
                                <div className="img-holder">
                                    {/* <label>Upload your picture</label> */}
                                   <img src={profileImg} alt="" id="img" className="img"/>
                               </div>
                               <input type="file" name="image-upload" id="input" accept="image/*" className="file" onChange={this.imageHandler}/>
                                
                           </div>

                       </div>
                        <div className="row row-a">
                            <div className="offset-1 col-lg-4 col-sm-12" >
                                <div className="form-group form-group-a">
                                    <label for="FirstName">First Name<span className="asterisk" style={{color:"red"}}> *</span></label>
                                        <input name= "FirstName" type="text" className="form-control" value={fields.FirstName}
                                        placeholder="Enter your first name"  onChange={event => this.handleUserInput(event)} />
<div>
    <span className="text-danger">{errors.FirstName}</span>
</div>
                                            {/* <small style={{color:"red",marginLeft:"0%"}}>{this.state.fnameerrormessage}</small> */}
                                </div>
                                
                            </div>
                            <div className="offset-1 col-lg-4 col-sm-12">
                                <div className="form-group form-group-a">
                                    <label for="LastName">Last Name<span className="asterisk" style={{color:"red"}}> *</span></label>
                                        <input name="LastName" type="text" className="form-control" value={fields.LastName}
                                        placeholder="Enter your last name"  onChange={event => this.handleUserInput(event)} /> 
<div>
    <span className="text-danger">{errors.LastName}</span>
</div>
                                        
                                       
                                </div>
                            </div>
             
                        </div>
                        <div className="row row-a">
                            
                            <div className="offset-1 col-lg-4 col-sm-12">
                                <div className="form-group form-group-a">
                                <label for="FatherName">Fathers Name<span className="asterisk" style={{color:"red"}}> *</span></label>
                                    <input name="FatherName" type="text" class="form-control" value={fields.FatherName}
                                    placeholder="Enter your father name" onChange={event => this.handleUserInput(event)} />
<div>
    <span className="text-danger">{errors.FatherName}</span>
</div>

                                       
                                </div>
                            </div>
                            <div className="offset-1 col-lg-4 col-sm-12" >
                                <div className="form-group form-group-a ">
                                <label for="MotherName">Mothers Name<span className="asterisk" style={{color:"red"}}> *</span></label>
                                    <input name="MotherName" type="text" class="form-control" value={fields.MotherName}
                                    placeholder="Enter your mother name" onChange={event => this.handleUserInput(event)} />
  <div>
    <span className="text-danger">{errors.MotherName}</span>
</div>                                     
                                </div>
                            </div>
             
                        </div>

                        <div className="row row-a">
                            
                            <div className="offset-1 col-lg-4 col-sm-12">
                                <div className="form-group form-group-a">
                                    <label for="DateOfBirth">Date of Birth<span className="asterisk" style={{color:"red"}}> *</span></label>
                                        <input name="DateOfBirth" type="date" class="form-control" value={fields.DateOfBirth}
                                        placeholder="Enter your date of birth" onChange={event => this.handleUserInput(event)} />    
                                        <small style={{color:"red",marginLeft:"0%"}}>{this.state.dobeerrormessage}</small>
                                </div>
                            </div>

                            <div className="offset-1 col-lg-4 col-sm-12" >
                                <div className="form-group form-group-a">
                                    <label for="Gender">Gender<span className="asterisk" style={{color:"red"}}> *</span></label>
                                
                                    <br></br><input type="radio" name="Gender"  value="Male" onChange={event => this.handleUserInput(event)} />Male &nbsp;

                                        <input type="radio" name="Gender"  value="Female" onChange={event => this.handleUserInput(event)} />Female
                                </div>
                            </div>
             
                        </div>

                        <div className="row row-a">
                            
                            <div className="offset-1 col-lg-4 col-sm-12">
                                <div className="form-group form-group-a">
                                    <label for="Email">Email ID<span className="asterisk" style={{color:"red"}}> *</span></label>
                                        <input  name="EmailId" type="email" class="form-control" value={fields.EmailId}
                                        placeholder="Enter your email id " onChange={event => this.handleUserInput(event)}/>
    <div>
    <span className="text-danger">{errors.EmailId}</span>
</div>

                                        
                                </div>
                            </div>
                            <div className="offset-1 col-lg-4 col-sm-12" >
                                <div className="form-group form-group-a">
                                    <label for="PhoneNo">Contact Number<span className="asterisk" style={{color:"red"}}> *</span></label>
                                        <input name="PhoneNo" type="text" class="form-control" value={fields.PhoneNo}
                                        placeholder="Enter your contact number" onChange={event => this.handleUserInput(event)}></input>
<div>
    <span className="text-danger">{errors.PhoneNo}</span>
</div>
                                        
                                </div>
                            </div>
             
                        </div>

                        <div className="row row-a">
                            
                            <div className="offset-1 col-lg-4 col-sm-12">
                                <div className="form-group form-group-a">
                                    <label for="AltContactNo">Alternate Contact Number</label>
                                    <input name="AltContactNo" type="text" class="form-control" value={fields.AltContactNo}
                                    placeholder="Enter Alternate Contact Number" onChange={event => this.handleUserInput(event)}/>
                                </div>
                            </div>
                            <div className="offset-1 col-lg-4 col-sm-12" >
                                <div className="form-group form-group-a ">
                                    <label for="Course">Course<span className="asterisk" style={{color:"red"}}> *</span></label>
                                    <select style={{ width: "100%" }} class="form-control" name="Course" onChange={event => this.handleUserInput(event)}>
                                            <option value="" selected disabled>Select your course</option>
                                             <option value="BE"  >BE</option>
                                             <option value ="Btech"  >Btech</option>
                                             <option value ="Mtech"  >Mtech</option>
                                        </select>
                                </div>
                            </div>
             
                        </div>

                        <div className="row row-a">
                            
                            <div className="offset-1 col-lg-4 col-sm-12" >
                                <div className="form-group form-group-a ">
                                    <label for="Department">Department<span className="asterisk" style={{color:"red"}}> *</span></label>
                                    <select style={{ width: "100%" }} class="form-control" name="Department" onChange={event => this.handleUserInput(event)} >
                                            <option value="" selected disabled>Select your department</option>
                                            <option value="Civil">Civil</option>
                                            <option value="CSE">CSE</option>
                                            <option value ="ECE">ECE</option>
                                            <option value = "IT">IT</option>
                                            <option value ="Mech">Mech</option>
                                                    </select>
                                </div>
                            </div>
                            <div className="offset-1 col-lg-4 col-sm-12">
                                <div className="form-group form-group-a">
                                    <label for="Semester">Semester<span className="asterisk" style={{color:"red"}}> *</span></label>
                                    <select style={{ width: "100%" }} class="form-control" name="Semester" onChange={event => this.handleUserInput(event)} >
                                        <option value="" selected disabled>Select your semester</option>
                                        <option value="I">I</option>
                                        <option value ="II">II</option>
                                        <option value ="III">III</option>
                                        <option value = "IV">IV</option>
                                        <option value="V">V</option>
                                        <option value="VI">VI</option>
                                        <option value="VII">VII</option>
                                        <option value="VIII">VIII</option>
                                    </select>
                                </div>
                            </div>
                            
             
                        </div>

                        <div className="row row-a">
                            
                            <div className="offset-1 col-lg-9 col-sm-12">
                                <div className="form-group form-group-a">
                                    <label for="Address">Address<span className="asterisk" style={{color:"red"}}> *</span></label>
                                        <textarea name="Address" rows="4" cols="25" className="form-control" 
                                        value={fields.Address} onChange={event => this.handleUserInput(event)} ></textarea>
<div>
    <span className="text-danger">{errors.Address}</span>
</div>
                                        
                                </div>
                            </div>
                            
                        </div>

                        <div className="row row-a">
                            
                            <div className="offset-1 col-lg-4 col-sm-12">
                                <div className="form-groupform-group-a">
                                    <label for="City">City<span className="asterisk" style={{color:"red"}}> *</span></label>
                                        <input name="City" type="text" className="form-control" value={fields.City}
                                        placeholder="Enter your city" onChange={event => this.handleUserInput(event)} />
<div>
    <span className="text-danger">{errors.City}</span>
</div>
                                        
                                </div>
                            </div>
                            <div className="offset-1 col-lg-4 col-sm-12" >
                                <div className="form-groupform-group-a ">
                                    <label for="State">State<span className="asterisk" style={{color:"red"}}> *</span></label>
                                        <input name="State" type="text" className="form-control" value={fields.State}
                                        placeholder="Enter your state" onChange={event => this.handleUserInput(event)}/>
<div>
    <span className="text-danger">{errors.State}</span>
</div>
                                </div>
                            </div>
             
                        </div>

                        <div className="row row-a">
                            
                            <div className="offset-1 col-lg-4 col-sm-12">
                                <div className="form-groupform-group-a">
                                <label for="CreatePassword">Create Password<span className="asterisk" style={{color:"red"}}> *</span></label>
                                    <input name="CreatePassword" type="password" class="form-control" value={fields.CreatePassword}
                                    placeholder="Enter your password" onChange={event => this.handleUserInput(event)} />
 <div>
    <span className="text-danger">{errors.CreatePassword}</span>
</div>                                           
                                </div>
                            </div>
                            <div className="offset-1 col-lg-4 col-sm-12" >
                                <div className="form-group form-group-a">
                                <label for="ReenterPassword">Re-enter Password<span className="asterisk" style={{color:"red"}}> *</span></label>
                                    <input name="Re_enterPassword" type="password" class="form-control"  value={fields.Re_enterPassword}
                                    placeholder="Re-enter your password" onChange={event => this.handleUserInput(event)} />
 <div>
    <span className="text-danger">{errors.Re_enterPassword}</span>
</div>                                         
                                </div>
                            </div>
             
                        </div>
                        <br/>
                        <div style={{textAlign:"center",alignContent:"center"}}>
                            <input  type="submit" value="Submit" className="btn btn-primary btn-lg" onClick={this.handleSubmit}></input>
                                
                        </div>

               
                
                            
                    </form>
                
                  
                    <br/>
              
            </div>
                <Footer/> 
            </div>
           
               
        )
    }
}

export default RegisterComponent