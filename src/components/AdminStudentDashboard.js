import React, { Component } from 'react'
import '../css/AdminStudentDashboard.css'

export class AdminStudentDashboard extends Component {


  
    
    render() {
        return (
            <div class="pagelayout">
                <div class="pagehead">
                    <h1><em>Student Dashboard</em></h1>
                    <hr></hr>
                    <div class="searchtab">
                    <label><b>Search : </b></label>
                    <input type="text"></input>
                    </div>
                    <div class="row ">
                    <div class="col-12">
                        <h2>Student Information</h2>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>ID</th>
                                        <th>Student Name</th>
                                        <th>Gender</th>
                                        <th>Course</th>
                                        <th>Department</th>
                                        <th>Year</th>
                                        <th>Semester</th>
                                        <th>Roll No.</th>
                                        <th>Email ID</th>
                                        <th>Contact No.</th>
                                        {/* <th>Fees Paid</th> */}
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>Davin John</td>
                                        <td>Male</td>
                                        <td>B.Tech</td>
                                        <td>Civil Engineering</td>
                                        <td>2017-2021</td>
                                        <td>VIII</td>
                                        <td>21</td>
                                        <td>john@gmail.com</td>
                                        <td>25498754</td>
                                        {/* <td>Yes</td> */}
                                        <td>
                                            <div class ="btn-group" role ="group">
                                                <a role="button" class="btn btn-warning">Edit</a>   
                                                <a role="button" class="btn btn-warning">Delete</a> 
                                            </div>
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Davin John</td>
                                        <td>Male</td>
                                        <td>B.Tech</td>
                                        <td>Civil Engineering</td>
                                        <td>2017-2021</td>
                                        <td>VIII</td>
                                        <td>21</td>
                                        <td>john@gmail.com</td>
                                        <td>25498754</td>
                                        {/* <td>Yes</td> */}
                                        <td><div class ="btn-group" role ="group">
                                                <a role="button" class="btn btn-warning">Edit</a>   
                                                <a role="button" class="btn btn-warning">Delete</a> 
                                            </div></td>
                                        
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Davin John</td>
                                        <td>Male</td>
                                        <td>B.Tech</td>
                                        <td>Civil Engineering</td>
                                        <td>2017-2021</td>
                                        <td>VIII</td>
                                        <td>21</td>
                                        <td>john@gmail.com</td>
                                        <td>25498754</td>
                                        {/* <td>Yes</td> */}
                                        <td><div class ="btn-group" role ="group">
                                                <a role="button" class="btn btn-warning">Edit</a>   
                                                <a role="button" class="btn btn-warning">Delete</a> 
                                            </div></td>
                                        
                                    </tr>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
           
       </div>

                </div>



               
            </div>
        )
    }
}

export default AdminStudentDashboard












// import React, { Component } from 'react'
// const $ = window.$;
// export class ReactCrud extends Component {
//     constructor(props){
//         super(props)
//             this.state = {
//                 name: '',
//                 address: '',
//                 email: '',
//                 contact: '',
//                 id: '', 
               
//                 data1: []
           
//             }
        
//     }
//     handleChange=(e)=> {
//         this.setState({[e.target.name]: e.target.value});
//         let nam=e.target.name;
//         let val=e.target.value;
//         let err="";

//         if (nam === "contact") {
//           if (val != "" && !Number(val)) {
//               err = <strong>Please enter valid phone number</strong>;
//           }
//           else if (val.length < 10) {
//             err = <strong>Please enter valid phone number</strong>;
//         }
//           this.setState({ errormessage: err });
//           this.setState({ [nam]: val });
//       }

//      if (nam === "name") {
//        if (val == "") {
//             err = <strong>Name cant be empty</strong>;
//        }
//        else if (Number(val)) {
//         err = <strong>Name cant be a number</strong>;
//        }
//         this.setState({ errormessage1: err });
//         this.setState({ [nam]: val });
//     }

//     if (nam === "address") {
//       if (val== "" ) {
//           err = <strong>Address cant be empty</strong>;
//       }
//       this.setState({ errormessage2: err });
//       this.setState({ [nam]: val });
//   }

//   if (nam === "email") {
//     if (val == "" ) {
//         err = <strong>Please enter Email</strong>;
//     }
//     this.setState({ errormessage3: err });
//     this.setState({ [nam]: val });
// }
      
      
//     }


    // componentDidMount() {
 
    //     $.ajax({
    //        url: "/getdata",
    //        type: "GET",
    //        dataType: 'json',
    //        ContentType: 'application/json',
    //        success: function(data) {         
    //          this.setState({data1: data}); 
             
    //        }.bind(this),
    //        error: function(jqXHR) {
    //          console.log(jqXHR);
               
    //        }.bind(this)
    //     });
    //   }
      
    // DeleteData(id){
    //   var studentDelete = {
    //         'id': id
    //            };      
    //     $.ajax({
    //       url: "/api/Removedata/",
    //       dataType: 'json',
    //       type: 'POST',
    //       data: studentDelete,
    //       success: function(data) {
    //         alert(data.data);
    //          this.componentDidMount();
    
    //       }.bind(this),
    //       error: function(xhr, status, err) {
    //          alert(err); 
               
              
    //       }.bind(this),
    //       });
    //     }
     
    //     EditData(item){         
    //    this.setState({name: item.name,address:item.address,contact:item.contact,email:item.email,id:item._id,Buttontxt:'Update'});
    //      }
    
    //    handleClick=()=> {
     
    //    var Url="";
    //    if(this.state.Buttontxt=="Save"){
    //       Url="/api/savedata";
    //        }
    //       else{
    //       Url="/api/Updatedata";
    //       }
    //       var studentdata = {
    //         'name': this.state.name,
    //         'address':this.state.address,
    //         'email':this.state.email,
    //         'contact':this.state.contact,
    //         'id':this.state.id,
            
    //     }
    //     $.ajax({
    //       url: Url,
    //       dataType: 'json',
    //       type: 'POST',
    //       data: studentdata,
    //       success: function(data) {       
    //           alert(data.data);       
    //           this.setState(this.state);
    //           this.componentDidMount();
             
    //       }.bind(this),
    //       error: function(xhr, status, err) {
    //          alert(err);     
    //       }.bind(this)
    //     });
    //   }
    
//     render() {
        
//         return (
//             <div>
//               <div  className="container"  style={{marginTop:'50px'}}>
//        <p className="text-center" style={{fontSize:'25px'}}><b> CRUD Opration Using React,Nodejs,Express,MongoDB</b></p>
//   <form>
    {/* <div className="col-sm-12 col-md-12"  style={{marginLeft:'400px'}}> 
  <table className="table-bordered">
     <tbody>
    <tr>
      <td><b>Name</b></td>
      <td>
         <input className="form-control" type="text" value={this.state.name}    name="name" onChange={ this.handleChange } required/>
         {this.state.errormessage1}
          <input type="hidden" value={this.state.id}    name="id"  />
      </td>
    </tr>

    <tr>
      <td><b>Address</b></td>
      <td>
      <input type="text" className="form-control" value={this.state.address}  name="address" onChange={ this.handleChange } required />
      {this.state.errormessage2}
      </td>
    </tr>

    <tr>
      <td><b>Email</b></td>
      <td>
        <input type="text"  className="form-control" value={this.state.email}  name="email" onChange={ this.handleChange } required/>
        {this.state.errormessage3}
      </td>
    </tr>


    <tr>
      <td><b>Contact</b></td>
      <td>
        <input type="text"  className="form-control" value={this.state.contact}  name="contact" onChange={ this.handleChange } required/>
        {this.state.errormessage}
      </td>
    </tr>

    <tr>
      <td></td>
      <td>
        <input className="btn btn-primary" type="button" disabled={!(this.state.name || this.state.address || this.state.email || this.state.contact)} value={this.state.Buttontxt} onClick={this.handleClick} />
      </td>
    </tr>

 </tbody>
    </table>
</div>   */}
  
{/* <div className="col-sm-12 col-md-12 "  style={{marginTop:'50px',marginLeft:'300px'}} >
 
 <table className="table-bordered"><tbody>
   <tr><th><b>S.No</b></th><th><b>NAME</b></th><th><b>ADDRESS</b></th><th><b>EMAIL</b></th><th><b>CONTACT</b></th><th><b>Edit</b></th><th><b>Delete</b></th></tr>
    {this.state.data1.map((item, index) => (
        <tr key={index}>
           <td>{index+1}</td> 
          <td>{item.name}</td>                      
          <td>{item.address}</td>
          <td>{item.email}</td>
          <td>{item.contact}</td>
           <td> 
          
           <button type="button" className="btn btn-success" onClick={(e) => {this.EditData(item)}}>Edit</button>    
          </td> 
          <td> 
             <button type="button" className="btn btn-info" onClick={(e) => {this.DeleteData(item._id)}}>Delete</button>
          </td> 
        </tr>
    ))}
    </tbody>
    </table>
     </div>
</form>         */}
//       </div>       
//       </div>
//         )
//     }
// }

// export default ReactCrud

