import React, { Component } from 'react'
import it from '../images/it_1.jpg'
import '../css/foot.css'
import Footer from './Footer'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
export class ITComponent extends Component {
    render() {
        return (
            <div><Breadcrumb>
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
            <Breadcrumb.Item >Department</Breadcrumb.Item>
            <Breadcrumb.Item active>it</Breadcrumb.Item>
          </Breadcrumb>
                <div className="container-fluid">
                    
                    <div class="media">
                        <img class="align-self-start mr-2 mt-5" src={it} alt="image" style={{ width: "20%" }} />
                        
                        <div className="media-body">
                        <h2>Information Technology</h2>
                            <h2 class="mt-2"> Vision</h2>
                                
                                <p>To establish the department as a major source of manpower for the IT sector.</p>
                            
                             <h2>Mission</h2>
                                <p>To produce engineering graduates with sound technical knowledge in Information Technology, good communication skills and ability to excel in professional career.</p>
                                
                                
                            <h2>Salient Features of Department</h2>
                            <ul>
                                <li>The department has been accredited twice by National board of Accreditation, AICTE.</li>
                                <li>Department has an excellent infrastructure. Laboratories are well equipped with computers of latest configuration and Internet facility. Latest software, wireless access point, LCD projectors and a separate router are used in the laboratories and for teaching purpose.</li>
                                <li>Students are motivated and encouraged to appear in GATE, CAT, GRE and other competitive examinations through guest lectures and seminars by eminent personalities in this field. This has resulted in increased number of students appearing and clearing these prestigious examinations.</li>
                            </ul>
                        </div>
                    </div>
                    
                    
                </div>
                    
                <Footer/>
            </div>
        )
    }
}

export default ITComponent