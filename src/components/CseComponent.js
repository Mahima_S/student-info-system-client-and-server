import React, { Component } from 'react'
import cse from '../images/cse_1.jpg'
import '../css/foot.css'
import Footer from './Footer'
import Breadcrumb from 'react-bootstrap/Breadcrumb'
export class CseComponent extends Component {
    render() {
        return (
            <div>            <Breadcrumb>
            <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
            <Breadcrumb.Item >Department</Breadcrumb.Item>
            <Breadcrumb.Item active>cse</Breadcrumb.Item>
          </Breadcrumb>
                   <div className="container-fluid">
                    
                    <div class="media">
                        <img class="align-self-start mr-2 mt-5" src={cse} alt="image" style={{ width: "20%" }} />
                        
                        <div className="media-body">
                        <h2>Computer Science Engineering</h2>
                            <h2 class="mt-2"> Vision</h2>
                                
                                <p>To continually improve the education environment, in order to develop graduates with strong academic and technical background needed to achieve distinction in the discipline. The excellence is expected in various domains like workforce, higher studies or lifelong learning. To strengthen links between industry through partnership and collaborative development works.</p>
                            
                             <h2>Mission</h2>
                                <p>To develop strong foundation of theory and practices of computer science amongst the students to enable them to develop into knowledgeable, responsible professionals, lifelong learners and implement the latest computing technologies for the betterment of the society.</p>
                    
                            <h2>Salient Features of Department</h2>
                            <ul>
                                <li>The Department of Computer Science & Engineering was established in 2002, is well-equipped with state-of-the-art infrastructure.</li>
                                <li>The state of art infrastructure includes latest configuration desktops organized in four different laboratories. There are total 170 desktops with internet facility and inter-connected by a 24 hours server and CISCO router.</li>
                                <li>Computer laboratories have IBM and WIPRO servers and uses software of industry standard like Rational Rose, Oracle, DB2 AIX, and MSDN subscription for Microsoft products.</li>
                            </ul>
                        </div>
                    </div>
                    
                    
                </div>
                    
                <Footer/>
            </div>
        )
    }
}

export default CseComponent