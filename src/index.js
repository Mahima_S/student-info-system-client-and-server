import React from 'react';
import ReactDOM from 'react-dom';
import {Route,BrowserRouter as Router, Link,Switch} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'font-awesome/css/font-awesome.css';
import 'bootstrap-social/bootstrap-social.css';
import './index.css';
import "./css/navbar.css";
import reportWebVitals from './reportWebVitals';

import App from './App';
import EceComponent from './components/EceComponent';
import CseComponent from './components/CseComponent';
import CivilComponent from './components/CivilComponent';
import MechComponent from './components/MechComponent';
import ITComponent from './components/ITComponent';

import NewsComponent from './components/NewsComponent'
import GalleryComponent from './components/GalleryComponent'
import ContactComponent from './components/ContactComponent'

import RegisterComponent from './components/RegisterComponent';
import LoginComponent from './components/LoginComponent';
import logo from './images/logo_1.PNG'
import NotfoundComponent from './components/NotfoundComponent';
import StudentDashboardComponent from './components/StudentDashboardComponent';
import AdminComponent from './components/AdminComponent';


const routing=(
<Router>
    <div >
      {/* <div className="col-12">
            <header  style={{padding:"20px"}} className="mainheader">
              <img src={classroom}  alt="logo" height="10%" width="10%" style={{marginLeft:"10%"}}/>
              <h1 style={{ display: "inline", marginLeft: "15%" }}>Student Information System</h1>
            </header>
          </div> */}

      <nav className="navbar navbar-expand-lg navbar-light bg-primary ">
        
        
        <div class="container-fluid">
        <div class="col-lg-3  navbar-header">
          <img src={logo}  alt="logo" height="10%" width="10%"/>
          <a class="navbar-brand" style={{color:"white",marginLeft:"10px"}} href="/">Student Information System</a>
        {/* </div>
          <div class="navbar-header"> */}
            <button type="button" class="navbar-toggler" style={{marginLeft:"10px"}}  data-toggle="collapse" data-target="#myNavbar" >
              <span class="navbar-toggler-icon" ></span>
              {/* <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>  */}
            </button>
          </div>       
    
        
          <div class="collapse navbar-collapse" id="myNavbar">
            <ul className="offset-2 nav navbar-nav">
              <li className="nav-item">
                <Link className="nav-link"   to="/">Home<i class="fa fa-home fa-1x" aria-hidden="true" style={{marginLeft:"7px"}}></i></Link>
              </li>
        
      
    
              <li className="nav-item">
                <Link className="nav-link"   to="/news">News<i class="fa fa-newspaper-o fa-1x" aria-hidden="true" style={{marginLeft:"7px"}}></i></Link>
              </li>
        
              <li className="nav-item dropdown">
                <Link className="nav-link dropdown-toggle" data-toggle="dropdown" id="navbarDropdownMenuLink" role="button"
                      aria-haspopup="true" aria-expanded="false" >Departments<i class="fa fa-building fa-1x" aria-hidden="true" style={{marginLeft:"7px"}}></i></Link>
                <ul className="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <li><Link className="dropdown-item"   to="/department/civil">Civil</Link></li>
                  <li><Link className="dropdown-item" to="/department/cse">CSE</Link></li>
                  <li><Link className="dropdown-item"   to="/department/ece">ECE</Link></li>
                  <li><Link className="dropdown-item" to="/department/it">IT</Link></li>
                  <li><Link className="dropdown-item" to="/department/mechanical">Mechanical</Link></li>
                  
                </ul>
              </li>
              
              <li className="nav-item">
                <Link className="nav-link"   to="/gallery">Gallery<i class="fa fa-picture-o fa-1x" aria-hidden="true" style={{marginLeft:"7px"}}></i></Link>
              </li>
      
              <li className="nav-item">
                <Link className="nav-link"   to="/contact">Contact<i class="fa fa-address-card fa-1x" aria-hidden="true" style={{marginLeft:"7px"}}></i></Link>
              </li>
            
              
            
              <li className="nav-item">
                <Link className="nav-link" to="/register">Register<i class="fa fa-user fa-1x" aria-hidden="true" style={{marginLeft:"7px"}}></i> </Link>
              </li>
              <li className="nav-item">
                <Link className="nav-link" to="/login">Login<i class="fa fa-sign-in fa-1x" aria-hidden="true" style={{marginLeft:"7px"}}></i> </Link>
              </li>
            </ul>
          
      
    
          </div>

        </div>
      </nav>
      <Switch>
        <Route exact path="/" component={App}/>
        <Route exact path="/news" component={NewsComponent} />
        <Route exact path="/department/civil" component={CivilComponent}/>
        <Route exact path="/department/cse" component={CseComponent}/>
        <Route exact path="/department/ece" component={EceComponent}/>
        <Route exact path="/department/it" component={ITComponent} />
        <Route exact path="/department/mechanical" component={MechComponent}/>
        <Route exact path="/gallery" component={GalleryComponent}/>
        <Route exact path="/contact" component={ContactComponent} />
        <Route exact path="/register" component={RegisterComponent}/>
        <Route exact path="/login" component={LoginComponent} />
        <Route exact path="/StudentDashboardComponent" component={StudentDashboardComponent} />
        <Route exact path="/AdminComponent" component={AdminComponent} />
        <Route component={NotfoundComponent}/>
      </Switch>
      

    </div>
</Router>
)

ReactDOM.render(routing,document.getElementById('root'));

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();